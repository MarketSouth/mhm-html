module.exports = {
  purge: [],
  theme: {
    extend: {
      colors: {
        sage: "#B9C4BA",
        navy: "#092340",
        darkTeal: "#234C5A",
        stone: "#E2DDCB",
        steel: "#E6E7E8",
      },
    },
  },
  variants: {},
  plugins: [],
};
